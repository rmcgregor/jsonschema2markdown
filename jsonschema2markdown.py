import argparse
import json


class JsonSchema2Markdown(object):
    def convert_object(self, jsn):
        print("|name|type|description|")
        print("|---|---|---|")
        for name, val in jsn["properties"].items():
            print(f"|{name}|{val['type']}|{val.get('description', '')}|")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("schema")

    args = parser.parse_args()

    with open(args.schema, "r") as f:
        schema = json.load(f)

    convertor = JsonSchema2Markdown()
    convertor.convert_object(schema)

if __name__ == '__main__':
    main()